{
  description = "manaleak project flake";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

  outputs = { self, nixpkgs }:{

    devShells.x86_64-linux.default = 
      with import nixpkgs {
        system = "x86_64-linux";
      };
      pkgs.mkShell {
        name = "python-shell";
      	packages = with pkgs; [
	        python310
	        python310Packages.easyocr
            python310Packages.opencv4
            python310Packages.flake8
            python310Packages.black
            python310Packages.yapf
            poetry
	];
	shellHook = "echo Welcome to the manaleak dev shell";
      };
    };
}

#!/usr/bin/env python3

import cv2
import numpy as np


def crop_title(card_image: np.ndarray) -> np.ndarray:
    '''Crop title of a mtg card image only based on standard coordinates.

    Params:
        card_image - opencv image of a standard mtg card.

    Returns:
        A crop version of the original image containing only the title
    '''
    height, width, _ = card_image.shape
    left = round(width / 15)
    right = round(width / 1.3)
    top = round(height / 25)
    bottom = round(height / 10)
    return card_image[top:bottom, left:right].copy()


if __name__ == "__main__":
    img = cv2.imread("../datasets/std/im1.jpg")
    cv2.imwrite("test.jpg", crop_title(img))

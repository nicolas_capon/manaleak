#!/usr/bin/env python3

import argparse
import easyocr

# Argument parsing
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="image path")
args = vars(ap.parse_args())

# Convert BGR to RGB channel
# this needs to run only once to load the model into memory
reader = easyocr.Reader(['en'])
result = reader.readtext(args["image"], detail=0)
print(result)

#!/usr/bin/env python3

import cv2
import os
import numpy as np


def load_dir(directory: str, imread_fl: int) -> list[np.ndarray]:
    '''Load all images from directory with given flag into a list

    Params:
        directory - file path of the directory
        imread_fl - flag for the imread function (ex: cv2.IMREAD_GRAYSCALE)

    Returns:
        List of opencv images
    '''
    image_files = []
    for filename in os.listdir(directory):
        # Ajoutez d'autres extensions d'image si nécessaire
        if filename.endswith('.jpg') or filename.endswith('.png'):
            image_files.append(os.path.join(directory, filename))

    images = []
    for image_file in image_files:
        image = cv2.imread(image_file, imread_fl)
        if image is not None:
            images.append(image)

    return images


def calculate_std(images: list[np.ndarray], threshold: int) -> np.ndarray:
    '''Calculate standard deviation intra-pixel in a list of images.
       Then perform thresholding to output a binary image representation of
       high variance zones.
       All images should have the same shape.

    Params:
        images - list of opencv images in grayscale
        threshold - int between 0 and 255 to threshold the binary image

    Returns:
        A binary opencv image

    Raises:
        ValueError: If an image has a different shape
    '''
    shape = None
    for image in images:
        if not shape:
            shape = image.shape
        elif image.shape != shape:
            raise ValueError(f"Image should have shape [{shape}]")
        image = cv2.blur(image, (5, 5))

    std = np.std(np.array(tuple(images)), axis=0)
    _, thresh = cv2.threshold(std, threshold, 255, cv2.THRESH_BINARY)

    return thresh


if __name__ == "__main__":
    images = load_dir("datasets/std", cv2.IMREAD_GRAYSCALE)
    im_std = calculate_std(images, 70)
    cv2.imwrite("std.jpg", im_std)
